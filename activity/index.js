//console.log("Hello World");

// S 20 - Javascript Repetition Control Structures

// ACTIVITY


/*
PART 1: 

	- Create a variable number that will store the value of the number provided by the user via the prompt.

	- Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

	- Create a condition that if the current value is less than or equal to 50, stop the loop.

	- Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

	- Create another condition that if the current value is divisible by 5, print the number.

*/

	// CODE HERE:

for (let count = 100; count >= 50; count --) {
    if (count % 10 === 0) {
        console.log("The number is divisible by 10. Skipping number.");
        continue;
    }
    if (count % 5 === 0) {
        console.log(count);
        continue;
    }
    if (count <= 50) {
        break;
    }
}


/*
PART 2:

	- Create a varaible that will contain the string "supercalifragilisticexpialidocious"

	- Create another varaible that will store the consonants from the string.

	- Create a for loop that will iterate through the individual letters of the string based on it's length.

	- Create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	- Create an else statement that will add the consonants to the second variable.


*/

	// CODE HERE:

console.log(" ");
let word = "supercalifragilisticexpialidocious";
console.log(word);
let consonants = "";
console.log(consonants)

for (x = 0; x < word.length; x++) {

    if (
        word[x].toLowerCase() == 'a' ||
        word[x].toLowerCase() == 'e' ||
        word[x].toLowerCase() == 'i' ||
        word[x].toLowerCase() == 'o' ||
        word[x].toLowerCase() == 'u'
    ) {
        continue;
    } else {
        consonants += word[x]
    }
}

console.log(consonants)